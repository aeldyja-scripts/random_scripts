#!/bin/bash

declare -r source_file=${1:?"You need to put a .mov file in arg1"}
declare -ri fps=${2:-25}

if [[ ! "${source_file}" =~ ^.*\.mov$ ]]; then
    printf 'You need to put a .mov file in arg1\n'
    exit 1
fi

mkdir build output
rm -r build/*
ffmpeg -i "${source_file}"  -r "${fps}" build/ffout%04d.png
convert -loop 0 -delay $((100 / fps)) -dispose Background build/ffout*.png "output/${source_file/.mov/.gif}"

rm -r build
