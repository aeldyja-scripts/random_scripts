#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
declare -r SCRIPT_DIR

recursive=false
extensions=('tgz' 'tar' 'tar.gz')
regex=""


# Execute getopt {{{
ARGS=$(getopt -o re:R: -l "recursive,extension:,regex:" -- "$@") || exit 1

eval set -- "${ARGS}";

while true; do
    case ${1} in
        -e|--extension)
            [[ -n "${2}" ]] && {
                extensions+=("${2}")
                shift 2
            } || exit 2;;
        -r|--recursive)
            recursive=true
            shift;;
        -R|--regex)
            [[ -n "${2}" ]] && {
                regex="${2}"
                shift 2
            } || exit 2;;
        --)
            shift
            break;;
    esac
done
#}}}

dir_to_cleanup="${1:?Missing directory to cleanup (arg1)}"
retention_date="${2:?Missing retention date (arg2)}"

#Check if directory is valid and is writable (doesn't check full recursivity)
[ -d "${dir_to_cleanup}" ] && { [ -w "${dir_to_cleanup}" ] || { echo "Invalid permissions in directory"; exit 1; } } || { echo "Path \"${dir_to_cleanup}\" is not a valid directory"; exit 1; }

#Calculate mtime and check validity
mtime=$(( ( $(date +%s) - $(date +%s -d "$(date --date="${retention_date}")") ) / 86400 ))
[[ ${mtime} -eq 0 ]] && exit 1

#Build extensions regex
if [ -z "${regex}" ]; then
    regex=".*\.\\($( IFS='|'; echo "${extensions[*]}" )\)"
    regex="${regex//|/\\|}"
fi

files_found=$(find "${dir_to_cleanup}" $(${recursive} || printf -- '-maxdepth 1') -regex "${regex}" -type f -mtime +${mtime} -print | wc -l)
[[ ${files_found} -eq 0 ]] && exit 0

echo "Backup Cleaner -- Deleting old files"
find "${dir_to_cleanup}" $(${recursive} || printf -- '-maxdepth 1') -regex "${regex}" -type f -mtime +${mtime} -print -delete

exit 0
