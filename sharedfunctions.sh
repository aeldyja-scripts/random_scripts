#!/bin/bash
#shellcheck disable=SC1083,SC1073

declare -r trace_message='echo $(basename $0) called from functions '"\'"'$(flist=${FUNCNAME[@]:1} && printf "${flist// /\<\<}")'"\'"' encountered a fatal error and exited :'
declare -ar level_list="(emerg alert crit err warning notice info debug)"

#Array {{{
function is_Array() {
declare -p "${1:?$(logExit '$1 not defined')}" 2> /dev/null | grep -q 'declare \-a'
return $?
}

function in_Array() {
array_Name="${1:?$(logExit '$1 not defined')}"
string_Name="${2:?$(logExit '$2 not defined')}"
#Check if parameter 1 is an array and parameter 2 is a string
is_Array $(printf "${array_Name}") && is_String $(printf "${string_Name}") && : || logExit "Invalid parameters \$1 need to be an Array and \$2 a string"
[[ "$(eval array=\( \${${array_Name}[@]} \) && echo ${array[*]})" == *"${!string_Name}"* ]] && return 0 || return 1
}

function get_index() {
array_Name="${1:?$(logExit '$1 not defined')}"
string_Name="${2:?$(logExit '$2 not defined')}"

#Check if parameter 1 is an array and parameter 2 is a string
is_Array $(printf "${array_Name}") && is_String $(printf "${string_Name}") && : || logExit "Invalid parameters \$1 need to be an Array and \$2 a string"

eval array=\( \${${array_Name}[@]} \)

for i in "${!array[@]}"; do
    if [[ "${array[$i]}" = "${!string_Name}" ]]; then
        printf "${i}";
    fi
done
}
#}}}

#String {{{
function is_String() {
declare -p "${1:?$(logExit '$1 not defined')}" 2> /dev/null | grep -q 'declare \-\-'
return $?
}
#}}}

#Int {{{
function is_Int() {
declare -p "${1:?$(logExit '$1 not defined')}" 2> /dev/null | grep -q "declare \-[a-zA-Z]*i[a-zA-Z]*"
return $?
}
#}}}

# Test Operator {{{
function isset() {
if [ -z ${1+x} ]; then
    return 1
else
    return 0
fi
}
#}}}

# Network {{{
function isIpLink() {
wget -q --tries=1 --timeout=20 --spider http://google.com
if [[ ! $? -eq 0 ]]
then
    logExit "Could not reach network" 33
fi
}

function Curl() {
curl --silent $1
}

function Wget() {
wget --quiet $1 -O $2
}

function getResponseCode() {
curl -sL -w "%{http_code}" "$1" -o /dev/null
}
#}}}

# Files {{{
function check_file() { #Parameters -- 1:(str)"path_to_file" *:FLAGS{IS_DIR,R,W,X} {{{
local CHECK_IS_DIR=false
local CHECK_R=false
local CHECK_W=false
local CHECK_X=false

#echo $@
local file="${1}"; shift

for flag in ${@}; do
    #echo $flag
    local var_name="CHECK_$flag"
    #echo "${!var_name}"
    if [ -z ${!var_name+x} ]; then
        printf 'Flag %s is invalid. FATAL ERROR\n' ${flag}
        exit 1
    else
        eval "${var_name}=true"
    fi
done

if [ ! -e "${file}" ]; then
    return 1
fi

if [ "${CHECK_IS_DIR}" == "true" ] && [ ! -d "${file}" ]; then
    return 2
fi
if [ "${CHECK_R}" == "true" ] && [ ! -r "${file}" ]; then
    return 3
fi
if [ "${CHECK_W}" == "true" ] && [ ! -w "${file}" ]; then
    return 4
fi
if [ "${CHECK_X}" == "true" ] && [ ! -x "${file}" ]; then
    return 5
fi
return 0
} #}}}

function searchInFile() { ## Usage : $1 file use, $2 patern to search
(cat ${1} | egrep -iq ${2}) && true || false
}

function readConfigFile() { ## usage : declare -A #FUNCTIONNAME#=eval "#FUNCTIONNAME#=$(readConfigFile #FilePATH#)"
local filepath=$1
local section
declare -A configData=([sections]='')
declare -a sections=()
declare -a subsections=()

if [[ -e $filepath && -s $filepath && -f $filepath ]];then
    if [ -r $filepath ];then
        while IFS='= ' read key val; do
            if [[ "$key" == \[*] ]]
            then
                if [[ "$section" && "$subsections" ]];then
                    declare configData["$section,subsections"]="$subsections"
                    subsections=()
                fi
                section="${key//\[}"
                section="${section//\]}"
                sections+="${section} "
            elif [[ "$val" ]]
            then
                if [[ "$section" ]]; then
                    subsections+="$key "
                    declare configData["$section,$key"]="$val"
                fi
            fi
        done < "$filepath"

        configData[sections]=${sections}

        returnValue="$(var_dump "configData")"
        returnValue=${returnValue//\</\\\<}
        returnValue=${returnValue//\>/\\\>}
        printf "${returnValue#'configData='}"
    else
        logExit "Can not read $filepath" 11
    fi
else
    logExit "File does not exist or is null : $filepath" 10
fi
}
#}}}

#Log {{{
function writeLog() {
#echo "1 : $1 2: $2 3: $3 4: $4" ; exit 0 #debug
message="${1:?$(logExit '$1 not defined')}"
level="${2:-info}"
unit="${3:-$(basename $0)}"
in_Array level_list level && systemd-cat -t "${unit}" -p "${level}" printf "${message}" || logExit "Invalid parameter \$2 must be one of the following '${level_list[*]}'"
}

function logExit() {
message="${1:?$(logExit '$1 not defined')}"
code="${2:-1}"
writeLog "$(eval ${trace_message}) ${message}" "debug" "icp-exit"
printf "${message}\n"
exit "${code}"
}
#}}}

# Other {{{
function user_exist() {
id -u "${1:?$(logExit '$1 not defined')}" > /dev/null 2>&1
}

function bin_check() {
bin_check=true
for p in $1; do
    which "${p}" > /dev/null 2>&1 || { echo "${p} cannot be found. Please install it."; bin_check=false; }
done
$bin_check || exit 3
}
#}}}

SHAREDFUNC=true
